
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(setq custom-file
      (expand-file-name "emacs-customize.el"
			(file-name-directory load-file-name)))
(load custom-file)

(require 'evil)
(evil-mode 1)
(evil-set-initial-state 'calendar-mode 'emacs)
(display-time)
