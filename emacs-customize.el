
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(custom-enabled-themes (quote (deeper-blue)))
 '(display-battery-mode t)
 '(display-time-day-and-date t)
 '(gnus-select-method (quote (nntp "news.gnus.org")))
 '(indent-tabs-mode nil)
 '(package-archives
   (quote
    (("gnu" . "https://elpa.gnu.org/packages/")
     ("marmalade" . "https://marmalade-repo.org/packages/")
     ("melpa" . "https://melpa.org/packages/")
     ("orgmode" . "https://orgmode.org/elpa/"))))
 '(package-selected-packages
   (quote
    (python-mode dockerfile-mode docker jdee markdown-mode markdown-mode+ toml-mode yaml-mode company company-go go-mode groovy-mode evil)))
 '(shell-file-name "/bin/bash")
 '(timeclock-mode-line-display t)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(package-initialize)
